\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "soprano"
		\clef "treble"
		\key e \minor

		R1  |
		r2 r4 b' 8 a'  |
		b' 8 e' e' e' 4 e' 8 e' d'  |
		e' 4 -\staccato e' g' b'  |
%% 5
		a' 4 a' 8 g' a' a' g' a' ~  |
		a' 8 a' g' a' 4 ( b' -\staccato ) r8  |
		e' 4 e' r8 e' e' b' ~  |
		b' 8 b' 4 r8 r a' g' b' ~  |
		b' 8 b' 2.. (  |
%% 10
		b' 4 a' g' ) b' 8 a'  |
		b' 4 e' 8 e' 4 e' 8 e' d'  |
		e' 4 -\staccato d' 8 e' 4 e' 8 b' b'  |
		a' 4 a' 8 g' a' a' g' a' ~  |
		a' 8 b' 2 r8 r4  |
%% 15
		b' 2 ~ b' 8 b' 4 b' 8  |
		b' 4 a' g' fis'  |
		e' 1  |
		r2 r8 e' e' e'  |
		e' 2 ( g'  |
%% 20
		b' 2 ) r8 e' e' e'  |
		d' 2 ( fis'  |
		a' 2 ) r8 d' d' d'  |
		c' 2 ( e'  |
		g' 2 ) r8 g' a' g'  |
%% 25
		fis' 8 ( g' fis' g' fis' g' fis' g'  |
		fis' 2 ) r8 e' e' e'  |
		e' 2 ( g'  |
		b' 2 ) r8 e' e' e'  |
		d' 2 ( fis'  |
%% 30
		a' 2 ) r8 d' d' d'  |
		c' 2 ( e'  |
		g' 2 ) r8 g' a' g'  |
		fis' 8 ( g' fis' g' fis' g' fis' g'  |
		fis' 2 ) r4 b' 8 a'  |
%% 35
		b' 8 e' e' e' 4 e' 8 e' d'  |
		e' 4 -\staccato e' g' b'  |
		a' 4 a' 8 g' a' a' g' a' ~  |
		a' 8 a' g' a' 4 ( b' -\staccato ) r8  |
		e' 4 e' r8 e' e' b' ~  |
%% 40
		b' 8 b' 4 r8 r a' g' b' ~  |
		b' 8 b' 2.. (  |
		b' 4 a' g' ) b' 8 a'  |
		b' 4 e' 8 e' 4 e' 8 e' d'  |
		e' 4 -\staccato d' 8 e' 4 e' 8 b' b'  |
%% 45
		a' 4 a' 8 g' a' a' g' a' ~  |
		a' 8 b' 2 r8 r4  |
		b' 2 ~ b' 8 b' 4 b' 8  |
		b' 4 a' g' fis'  |
		e' 1  |
%% 50
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		Nues -- tra for -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.

		Re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		¡a -- le -- lu -- ya! __

		Re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		¡a -- le -- lu -- ya! __

		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		Nues -- tra for -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.
	}
>>
