\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "bass"
		\clef "treble_8"
		\key e \minor

		R1  |
		r2 r4 b 8 a  |
		b 8 e e e 4 e 8 e d  |
		e 4 -\staccato e g b  |
%% 5
		a 4 a 8 g a a g a ~  |
		a 8 a g a 4 ( b -\staccato ) r8  |
		e 4 e r8 e e b ~  |
		b 8 b 4 r8 r a g b ~  |
		b 8 b 2.. (  |
%% 10
		b 4 a g ) b 8 a  |
		b 4 e 8 e 4 e 8 e d  |
		e 4 -\staccato d 8 e 4 e 8 b b  |
		a 4 a 8 g a a g a ~  |
		a 8 b 2 r8 r4  |
%% 15
		b 2 ~ b 8 b 4 b 8  |
		b 4 a g fis  |
		e 1  |
		R1  |
		r2 b 4 ( a  |
%% 20
		g 2 ~ g 8 ) r r4  |
		r2 fis 4 ( g  |
		a 2 ~ a 8 ) r r4  |
		r2 c' 4 ( b  |
		c' 2 ~ c' 8 ) r r4  |
%% 25
		fis 2 ( c'  |
		b 2 ) r  |
		r2 b 4 ( a  |
		g 2 ~ g 8 ) r r4  |
		r2 fis 4 ( g  |
%% 30
		a 2 ~ a 8 ) r r4  |
		r2 c' 4 ( b  |
		c' 2 ~ c' 8 ) r r4  |
		fis 2 ( c'  |
		b 2 ) r4 b 8 a  |
%% 35
		b 8 e e e 4 e 8 e d  |
		e 4 -\staccato e g b  |
		a 4 a 8 g a a g a ~  |
		a 8 a g a 4 ( b -\staccato ) r8  |
		e 4 e r8 e e b ~  |
%% 40
		b 8 b 4 r8 r a g b ~  |
		b 8 b 2.. (  |
		b 4 a g ) b 8 a  |
		b 4 e 8 e 4 e 8 e d  |
		e 4 -\staccato d 8 e 4 e 8 b b  |
%% 45
		a 4 a 8 g a a g a ~  |
		a 8 b 2 r8 r4  |
		b 2 ~ b 8 b 4 b 8  |
		b 4 a g fis  |
		e 1  |
%% 50
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		Nues -- tra for -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.

		Oh, __ oh, __ oh, __ ah. __
		Oh, __ oh, __ oh, __ ah. __

		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		Nues -- tra for -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.
	}
>>
