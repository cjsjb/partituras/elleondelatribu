\context Staff = "baritono" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Barítono"
	\set Staff.shortInstrumentName = "B."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-baritono" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "bass"
		\clef "treble_8"
		\key e \minor

		R1  |
		r2 r4 e 8 e  |
		e 8 e e e 4 e 8 e e  |
		e 4 -\staccato e e e  |
%% 5
		d 4 d 8 c d d c d ~  |
		d 8 d c d 4 ( e -\staccato ) r8  |
		c 4 c r8 c c b, ~  |
		b, 8 b, 4 r8 r b, b, b, ~  |
		b, 8 b, 2.. (  |
%% 10
		b, 4 a, g, ) r  |
		e 4 e 8 e 4 e 8 e e  |
		e 4 -\staccato e 8 e 4 e 8 e e  |
		d 4 d 8 c d d c d ~  |
		d 8 e 2 r8 r4  |
%% 15
		c 2 ~ c 8 c 4 c 8  |
		b, 4 b, fis dis  |
		e 1  |
		R1  |
		r2 e ( ~  |
%% 20
		e 1  |
		d 1 ~  |
		d 2. ) r4  |
		r2 c ~  |
		c 1  |
%% 25
		b, 1 ~  |
		b, 2 r  |
		r2 e ( ~  |
		e 1  |
		d 1 ~  |
%% 30
		d 2. ) r4  |
		r2 c ~  |
		c 1  |
		b, 1 ~  |
		b, 2 r4 e 8 e  |
%% 35
		e 8 e e e 4 e 8 e e  |
		e 4 -\staccato e e e  |
		d 4 d 8 c d d c d ~  |
		d 8 d c d 4 ( e -\staccato ) r8  |
		c 4 c r8 c c b, ~  |
%% 40
		b, 8 b, 4 r8 r b, b, b, ~  |
		b, 8 b, 2.. (  |
		b, 4 a, g, ) r  |
		e 4 e 8 e 4 e 8 e e  |
		e 4 -\staccato e 8 e 4 e 8 e e  |
%% 45
		d 4 d 8 c d d c d ~  |
		d 8 e 2 r8 r4  |
		c 2 ~ c 8 c 4 c 8  |
		b, 4 b, fis dis  |
		e 1  |
%% 50
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-baritono" {
		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		%Nues -- tra
		             "...for" -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.

		Oh, __ oh, __ ah. __
		Oh, __ oh, __ ah. __

		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		%Nues -- tra
		             "...for" -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.
	}
>>
