\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "mezzosoprano"
		\clef "treble"
		\key e \minor

		R1  |
		r2 r4 b' 8 a'  |
		b' 8 e' e' e' 4 e' 8 e' d'  |
		e' 4 -\staccato e' e' e'  |
%% 5
		d' 4 d' 8 c' d' d' c' d' ~  |
		d' 8 d' c' d' 4 ( e' -\staccato ) r8  |
		c' 4 c' r8 c' c' e' ~  |
		e' 8 e' 4 r8 r g' g' g' ~  |
		g' 8 fis' 2.. (  |
%% 10
		g' 4 fis' e' ) b' 8 a'  |
		b' 4 e' 8 e' 4 e' 8 e' d'  |
		e' 4 -\staccato d' 8 e' 4 e' 8 e' e'  |
		d' 4 d' 8 c' d' d' c' d' ~  |
		d' 8 e' 2 r8 r4  |
%% 15
		c' 2 ~ c' 8 c' 4 c' 8  |
		b 4 b dis' dis'  |
		e' 1  |
		r2 r8 e' e' e'  |
		e' 2 ( b  |
%% 20
		e' 2 ) r8 e' e' e'  |
		d' 2 ( a  |
		d' 2 ) r8 d' d' d'  |
		c' 2 ( g  |
		c' 2 ) r8 c' d' c'  |
%% 25
		b 8 ( c' b c' b c' dis' e'  |
		dis' 2 ) r8 e' e' e'  |
		e' 2 ( b  |
		e' 2 ) r8 e' e' e'  |
		d' 2 ( a  |
%% 30
		d' 2 ) r8 d' d' d'  |
		c' 2 ( g  |
		c' 2 ) r8 c' d' c'  |
		b 8 ( c' b c' b c' dis' e'  |
		dis' 2 ) r4 b' 8 a'  |
%% 35
		b' 8 e' e' e' 4 e' 8 e' d'  |
		e' 4 -\staccato e' e' e'  |
		d' 4 d' 8 c' d' d' c' d' ~  |
		d' 8 d' c' d' 4 ( e' -\staccato ) r8  |
		c' 4 c' r8 c' c' e' ~  |
%% 40
		e' 8 e' 4 r8 r g' g' g' ~  |
		g' 8 fis' 2.. (  |
		g' 4 fis' e' ) b' 8 a'  |
		b' 4 e' 8 e' 4 e' 8 e' d'  |
		e' 4 -\staccato d' 8 e' 4 e' 8 e' e'  |
%% 45
		d' 4 d' 8 c' d' d' c' d' ~  |
		d' 8 e' 2 r8 r4  |
		c' 2 ~ c' 8 c' 4 c' 8  |
		b 4 b dis' dis'  |
		e' 1  |
%% 50
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		Nues -- tra for -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.

		Re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		¡a -- le -- lu -- ya! __

		Re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		re -- su -- ci -- tó, __
		¡a -- le -- lu -- ya! __

		El le -- ón de la tri -- bu de Ju -- dá,
		Je -- sús ven -- ció las ca -- de -- nas y nos li -- be -- ró, __
		Él es nues -- "tra an" -- tor -- cha de vic -- to __ ria. __

		Nues -- tra for -- ta -- le -- "za en" tiem -- pos de fla -- que -- za,
		u -- na to -- rre en tiem -- pos de gue -- rra,
		oh, "la es" -- pe -- ran -- za "de Is" -- ra -- el.
	}
>>
