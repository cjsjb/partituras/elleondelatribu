\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	e1:m e1:m

	% el leon de la tribu de juda'...
	e1:m e1:m d1 d1
	c1 g1 b1:7 b1:7

	% nuestra fortaleza...
	e1:m e1:m d1 d1
	c1 b1:7 e1:m e1:m

	% resucito'...
	e1:m r1*7

	% resucito'...
	e1:m e1:m d1 d1
	c1 a1:m7 b1:7 b1:7

	% el leon de la tribu de juda'...
	e1:m e1:m d1 d1
	c1 g1 b1:7 b1:7

	% nuestra fortaleza...
	e1:m e1:m d1 d1
	c1 b1:7 e1:m e1:m
	}
